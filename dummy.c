#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pico/stdlib.h"
#include "eproms.h"

#define TIME_PROG_US    0 
#define MAX_PROG_CYCLES 0
#define COMP_NAME       "UNBEKANNT"

static void print_warning() {
    printf("EPROM-Typ nicht ausgewaehlt !!! ==> 't'\n");
}

int check_dummy() {
    print_warning();
    return -1;
};

uint32_t get_dummy(uint8_t *buf) {
    print_warning();
    return 1;
};

bool burn_dummy(uint8_t *buf, int *cycles) {
    print_warning();
    *cycles = -1;
    return false;
};

rule_def_t rules_dummy() {
    rule_def_t r;
    bzero(r.name, sizeof(r.name));
    memcpy(r.name, COMP_NAME, sizeof(COMP_NAME));
    r.time_prog_plse_us = TIME_PROG_US;
    r.max_cycles = MAX_PROG_CYCLES;
    return r;
}

//Ändern der voreingesellten Programmierimpulsdauer
void set_prog_time_us_dummy (uint16_t time_us) {
    print_warning();
}

//Ändern der voreingestellten maximalen Programmierzyklen
void set_max_cycles_dummy (uint8_t cyc) {
    print_warning();
}

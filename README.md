
## 1. Motivaion
In einem Retro-Computer-Projekt wird ein gebrannter EPROM benötigt.
Meine Brenner aus den 80/90-er Jahren sind entweder nicht mehr vorhanden oder erfordern derart alte Betriebssysteme und Schnittstellen, dass eine Wiederbelebung nicht sinnvoll ist.<br>
Die Anschaffung eines neuen Gerätes kommt auf Grund der wenigen zu erwartenden Brennvorgänge nicht in Frage.<br>

Es soll daher ein EPROM-Brenner für eine begrenzte Anzahl von Typen "on the fly" aufgebaut werden.
Um den Aufwand gering zu halten, werden alle Teile auf einem Breadboard zusammengesteckt.<br>

Im Kern kommen Komponenten zum Einsatz, die ohnehin für dieses Retro-Projekt vorhanden sind. 
Lediglich einige diskrete Bauelemente aus Restbeständen werden zusätzlich benötigt.<br>

## 2. Vorstellung
Kernstück des EPROMers ist ein Raspberry Pi Pico, der auch Namensgeber des Projektes ist.
Gebrannt werden sollen die Typen: 
 - 27C64
 - 27C128
 - 27C256<br> 

Diese drei EPROMs sind weitgehend pinkompatibel und die Programmiervorschriften unterscheiden sich nur geringfügig.
Kleinere Typen sind kaum noch beschaffbar. Bei größeren EPROMs wird es knapp mit den GPIO des Pico.
Alle 26 verfügbaren GPIO sind schon beim 27C256 belegt.
Bereits beim 27C512 wären Schaltungsänderungen notwendig.<br>

Gesteuert wird der Pico über einen per USB verbundenen PC auf dem ein beliebiges serielles Terminalprogramm (mit gewissen Mindestanforderungen) läuft.<br>
Unter Linux kann "CuteCom" verwendet werden, das es auch für den Mac geben soll. In der Windows-Welt wird sich etwas geeignetes finden. Mit "Putty" oder "TeraTerm" sollte es funktionieren.
Ohne Bindung an eine spezielle Rechenarchitektur besteht die Hoffnung, dass das Ganze auch in einigen Jahren noch funktioniert.<br>

Die zu brennenden Daten müssen im Intel-Hex-Format auf den Pico hochgeladen werden. Ein Format aus der PC-Steinzeit, das die Jahre überdauert hat. Zumindest unter Linux lässt sich jede Binärdatei auf der Kommandozeile in ein I08Hex-File konvertieren.<br>

Die Programmierspannung (i.A. 12.5V) liefert ein externes (Labor-) Netzteil.

Schaltbild und Quellcode sind im Repository zu finden. Es steht auch das fertige UF2-File bereit. Einen Fritzing-Plan wird es nicht geben.<br>

Noch ein Hinweis in eigener Sache:<br>
Jeder ist eingeladen nachzubauen, umzubauen oder neu zu erfinden. In keinem Fall werde ich Haftung für irgendwelche Schäden übernehmen. Insbesondere bei der Freiverdrahtung können leicht Fehler passieren.
Aber auch EPROM-Typen mit abweichenden Programmiervorschriften oder -spannungen können Schaden nehmen.
Also -> Erst nachdenken, dann losbasteln.

## 3. Schaltung
Die Schaltung wird per USB-Port mit 5V versorgt. Der Pico generiert daraus seine internen 3.3V.
Für die Eingänge der CMOS-EPROMs stellt dies kein Problem dar, >= 2V werden als H-Pegel erkannt.
Eine direkte Verbindung von Adress- und Steuerleitungen ist somit möglich.
Anders sieht es bei den Datenleitungen aus. Wird der EPROM gelesen, liegen an den Datenausgängen ca. 5V.
Diese Spannung ist an den Pico-Eingängen nicht zulässig. Daher sind zwei 4-fach Levelshifter zwischengeschaltet, die es als Zehnerpack im Versandhandel günstig gibt. Die Funktion der GND-Anschlüsse auf diesen Boards ist unklar. Da sie schon mal da sind, wurden sie trotzdem auf Masse gelegt.<br>

Die Vcc des EPROMs wird über D1 entkoppelt. Dabei fallen je nach Typ 0,2V..0,5V ab. Die geringere Spannung ist kein Nachteil. Lt. Datenblättern soll das Vergleichslesen bei abgesenkter Spannung erfolgen.<br>

Zu Beginn des Programmiervorgangs wird die Programmierspannung an Vpp gelegt und Vcc auf 6V angehoben.
H-Pegel an GPIO15 steuert T3 und in Folge T1 durch. Damit liegt die Programmierspannung am Drain von T1.<br>

T2 arbeitet als Spannungsteiler. P1 wird so eingeregelt, dass ohne EPROM die lt. Datenblatt max. zulässige Spannung (6,25V) anliegt. Bei gestecktem EPROM wird die Spannung weiter belastet und sinkt dadurch noch etwas. R5 dient als Grundlast.
Zum Einregeln kann die Verbindung an GPIO15 abgezogen und fest auf 3,3V oder 5V gelegt werden.<br>

Am Ende jedes Programmiervorgangs wird GPIO15 nach unten gezogen und der Schaltungsteil geht in Grundstellung.<br>

Die Auswahl der Bauelemente ist unkritisch. Lediglich für T1 sollte ein Schalt-PMOS mit geringem R(ds) gewählt werden.<br>

Die Verdrahtung auf dem Breadboard ist nicht unerheblich. Die Zuordnung der Pins muss gewissenhaft ausgeführt und geprüft werden. Vertauschte Adress- oder Datenleitungen werden zu Fehlern führen, die auch beim Vergleichslesen nicht zu erkennen sind.<br>

Der Pico muss mit mindestens einem seiner GND-Pins mit der Schaltungsmasse verbunden sein. Hier gilt der Grundsatz, viel hilft viel. Ggf kann in der 12.5V-Zuleitung ein Stütz-Elko sinnvoll sein.

## 4. Bedienung 
Spezielle Treiber werden unter Linux und Windows nicht benötigt. Auf dem Mac ist das nicht geprüft. Nach dem Anstecken sollte sich der Pico als neue Schnittstelle am PC melden.
- Linux: "/dev/ttyACM0" 
- Windows: "COM3"<br><br>

Die Nummern können abweichen, je nachdem, ob bereits ein Gerät angeschlossen war. Auch das wiederholte Verbinden kann zu einer neuen Nummerierung führen.<br>

Hier das Vorgehen am Beispiel von Cutecom unter Linux (Mint 21).<br>
Für die Berechtigung zum Zugriff auf die serielle Schnittstelle muss der Linux-User Mitglied der Gruppe "dialout" sein.
Die Einstellungen in Cutecom müssen zur seriellen USB-Kommunikation passen. Im Zweifelsfall sind "115200bd 8N1 NoFlowcontrol" eine geeignete Wahl. Gesendete Zeilen sind durch "LF" abzuschließen<br>
Mit dem "Open"-Button wird die Verbindung hergestellt. Eine erfolgreiche Verbindung wird durch die On-Board-LED des Pico angezeigt. Im Terminal-Fenster sollte sich der Pico wie folgt melden:

```
picoBurner ist verbunden
 
Auswahl EPROM-Typ erforderlich
------------------------------
  --> t1 : 27C64
  --> t2 : 27C128
  --> t3 : 27C256
```
<br>
Das Trennen der Verbindung wird durch die LED nicht gezeigt.<br>

Gesteuert wird das Brennprogramm durch das Senden von Befehlszeilen die an der 1.Position einen Kennbuchstaben haben und mit "LF" abgeschlossen werden. Einige Befehle erfordern numerische Argumente. Diese können durch Leerzeichen vom Kennbuchstaben getrennt werden. Sämtliche Kennbuchstaben sind Kleinbuchstaben.<br>
Folgende Befehle sind möglich:

  - "t n" : Auswahl des EPROM-Typs - Weglassen des Arguments oder n=0 zeigt die verfügbaren Optionen an.
  - "c"   : ClearCheck- Prüft, ob alle Speicherzellen des gesteckten EPROM mit "FF" belegt sind.
  - "u"   : Upload eines IHex-Files initialisieren, s.u.
  - "q"   : Abbruch des Ladens
  - "m"   : Hexdump des hochgeladenen Files
  - "p"   : EPROM programmieren
  - "d"   : Hexdump des EPROM
  - "i nnnn" : Programmierimpulsdauer in µs (default 500µs)
  - "z nn"<tab>: Max Anzahl Programmierzyklen (default 20)<br><br>

  Programmierimpulsdauer und Anzahl der maximalen Zyklen müssen nur bei Abweichung von den Defaultwerten neu gesetzt werden.

  Ein Brennvorgang sollte wie folgt aussehen:

    - im spannungsfreien Zustand prüfen, ob alle Verbindungen gesteckt sind
    - EPROM stecken
    - Pico mit dem PC per USB verbinden
    - ser. Terminal starten, Einstellungen prüfen und serielle Verbindung öffnen
    - "t n" EPROM-Typ auswählen
    - "c" testen, ob der EPROM gelöscht ist
    - "u" laden eines Hexfiles initialisieren
    - File-Upload, s.u.
    - Programmierspannung anstecken
    - "p" Programmierung starten
    - nach Fertig- oder Fehlermeldung Programmierspannung trennen
    - ggf "d", um die programmierten Daten stichprobenartig zu prüfen.<br><br>

### 4.1 File - Upload
Als Dateiformat zum Hochladen der zu programmierenden Daten wurde das Intel-Hex-Format gewählt.
In diesem Format werden die Binärdaten byteweise in zweistellige Strings gewandelt, in Zeilen verpackt, sowie mit Adressinformationen und Prüfsummen versehen. Am Zeilenanfang wird ein Doppelpunkt vorangestellt.<br>

Da dieser Brenner maximal 16-Bit-Adressen verarbeitet, wurde das ursprüngliche I08Hex-Format gewählt. Zulässig sind die Satztypen 1 und 2. Spätere Weiterentwicklungen des Formates werden hier nicht berücksichtigt.<br>
Mit dem Kennbuchstaben "u" wird das Hochladen initialisiert. Dabei wird der Datenpuffer vollständig gelöscht, d.h. es ist nicht möglich, mehrere Datenblöcke in verschiedene Adressbereiche nacheinander zu laden. Für diesen Zweck muss das Hex-File entsprechend angelegt werden.<br>
Nach der Initialisierung mit "u" akzeptiert das Programm nur noch Zeilen, die mit ":" beginnen, ansonsten werden Fehlermeldungen ausgegeben. Einzige Ausnahme ist das "q", mit dem das Warten auf Daten abgebrochen wird.<br>
Der Upload erfolgt im Terminalprogramm als normale Textdatei, in Cutecom mit der Einstellung "plain".
Auswahlen wie X-, Y- oder Z-Modem wären falsch.

### 4.2 Hexfile bauen
Unter Linux kann die Konvertierung eines Binärfiles in ein IntelHexFile auf der Kommandozeile mit dem Befehl "srec_cat" erfolgen. Dieses Tool ist im Paket "srecord" enthalten und wie folgt zu installieren:
```
sudo apt install srecord
```
Am besten, in dem Verzeichnis, in dem sich die Binärdatei befindet, ein Terminalfenster (nicht das serielle Terminal) öffnen und folgenden Befehl eingeben:
```
srec_cat FILENAME_IN.bin -binary -offset 0x0000 -output FILENAME_OUT.hex -intel -address-length=2
```
Die Filenamen sind entsprechend anzupassen. Es ist auch möglich, mehrere Binärdateien in verschiedene Adressbereiche zu packen.
```
srec_cat FILE1.bin -binary -offset 0x0000 FILE2.bin -binary -offset 0x1000 -output FILE_OUT.hex -intel -address-length=2
```

Das Ausgabefile sollte im Texteditor so ähnlich aussehen:
```
:20000000ED737718181DFFFFC30218E5C5C3CA00C30518F5E5D51851C30818E37E23183201
:20002000C30B183156181803C30E18CDA5011803C31118C34C02FFFFC31418226F18E12213
:2000400056182A6F18ED737718315618CDA501C30501D7CB7F28C5E3C9DB04CB7FC8CD1D22
:2000600001DB04CB7FC9C31718E67F2A0018FE0D2837FE0C283AFE08284BFE203827772B86
:20008000EB21FF0FA7ED52EB2200183818C521BF1711FF1701C007EDB8ED530018EB232D08
:... weitere Datensätze ...
:00000001FF
```
Bis auf die letzte enthalten alle Zeilen Daten (Satztyp 0). Nur die letzte Zeile ist vom Typ 1 und zeigt das Dateiende an.
Eine Erklärung des Formats ist [hier](https://de.wikipedia.org/wiki/Intel_HEX) zu finden.


Viel Erfolg beim Brennen !<br>
Jörg / DL7VMD


   





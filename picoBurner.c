#include <stdio.h>
#include <stdlib.h>
#include <tusb.h>

#include "pico/stdlib.h"

#include "eproms.h"

// -----------------------------------------------

#define PICO_LED_IO 25

#define CR		    13
#define LF          10

#define SZ_BUF      0x10000 //64k f. max 16Bit-Adressbereich

enum components_t component;

//Prototypes
void set_component(enum components_t comp);
void print_comp_types();
void hexdump(uint8_t *buf, uint32_t len);

int main() {

    stdio_usb_init(); // nur USB initialisieren

    gpio_init(PICO_LED_IO);
    gpio_set_dir(PICO_LED_IO, GPIO_OUT);
    gpio_put(PICO_LED_IO, 0);
    
    if(!tud_cdc_connected()) {
        gpio_put(PICO_LED_IO, 0);
        while (!tud_cdc_connected()) sleep_ms(100); 
        printf("picoBurner ist verbunden\n\n");
    }
    gpio_put(PICO_LED_IO, 1); 

    printf("Auswahl EPROM-Typ erforderlich\n");
    printf("------------------------------\n");
    print_comp_types();

// ---------------------------------------------------------------

    uint8_t buf_burn[SZ_BUF];
    int lp = 0;

    memset(buf_burn, 0xFF, sizeof(buf_burn));

    char line[1024]; // I08HEX - Zeile
    char *result;
    char wx[5];
    uint16_t adr;       //max 16-Bit-Adressierung
    uint8_t typ;        //I08HEX - Satztyp (0 und 1 zulässig)
    uint8_t bx;
    uint8_t csum_ist , csum_soll;   //I08-Checksumme
    bool wait_for_ihex = false;     //Warten auf IHex-Zeile, alle anderen Eingaben werden abgewiesen
    uint16_t len_load;
    uint32_t max_addr = 0;
    int x;
    uint8_t rx_buf[SZ_BUF];
    uint32_t sz;
    bool b;
    int cyc;
    uint32_t arg;
    
    memchr(buf_burn, 0xFF, sizeof(buf_burn));

    set_component(E_UNDEFINED);

    while (true) {
        if ((result = gets(line)) != NULL){
            arg = atoi(&line[1]); //ggf Zahlenargument    
            switch(line[0]) {
                case ':' :  //Intel-Hex_line
                    if (!wait_for_ihex){
                        printf("Laden wurde nicht initialisiert --> press 'l'\n");    
                        break;
                    }    
                    if (len_load == 0) printf("Daten werden gelesen *");
                    else printf("*");    
                    //Datenlänge 2 Zeichen
                    bzero(wx, 5);
                    memcpy(wx, &line[1], 2);
                    sz = strtol(wx, NULL, 16);
                    csum_ist = sz;
                    //Adresse 4 Zeichen
                    memcpy(wx, &line[3], 4);
                    adr = strtol(wx, NULL, 16);
                    uint16_t adrx = adr + sz;
                    csum_ist += adr >> 8;
                    csum_ist += adr & 0x00FF;
                    //Satztyp 2 Zeichen [0..1]
                    bzero(wx, 5);
                    memcpy(wx, &line[7], 2);
                    typ = strtol(wx, NULL, 16);
                    csum_ist += typ;
                    //Datenblock
                    if (typ == 0) {     
                        for (int i=0; i<sz; i++) {
                            memcpy(wx, &line[i*2 + 9], 2);
                            //Byte in Datenpuffer schreiben
                            bx = strtol(wx, NULL, 16);
                            buf_burn[adr+i] = bx;
                            csum_ist += bx;
                        }
                        len_load += sz;
                        //maximale genutzte Adresse im Puffer / EProm
                        if ((adr + sz) > max_addr) max_addr = adr + sz - 1;    
                    }
                    //Prüfsumme 2 Zeichen
                    memcpy(wx, &line[sz*2 + 9], 2);
                    csum_soll = strtol(wx, NULL, 16);
                    csum_ist = ~csum_ist + 1;

                    if (csum_soll != csum_ist) {
                        printf("\nPruefsummenfehler - Abbruch\n");
                        wait_for_ihex = false;
                        break;
                    }
                    if (typ == 1) { //End of File
                        printf("\n%d Datenbytes erfolgreich gelesen | Max Adress: 0x%.4X\n", len_load, max_addr);
                        wait_for_ihex = false;
                    }
                    break;

                case 'u' :  //Upload Intel-Hex-File
                    if (wait_for_ihex) break;
                    printf("Initialisierung Puffer\n");
                    printf("Warten auf Intel-Hexfile...\n");
                    memchr(buf_burn, 0xFF, sizeof(buf_burn));
                    len_load = 0;
                    max_addr = 0;
                    wait_for_ihex = true;    
                    break;

                case 'q' : //Quit Hex-File laden
                    if (!wait_for_ihex) break;
                    printf("Abbruch\n");
                    wait_for_ihex = false;
                    break;

                case 'm' :  //Memory-Dump   
                    if (wait_for_ihex) break; 
                    if (max_addr == 0) {
                        printf ("Datenpuffer leer\n");
                        break;
                    }
                    hexdump(buf_burn, max_addr);
                    break;

                case 'c' :  //Check EPROM - Löschtest
                    if (wait_for_ihex) break;
                    
                    x = check_eprom();
                    printf("Pruefung EPROM ...\n");
                    if (x == 0) printf("EPROM geloescht\n");
                    else printf("%d Bytes nicht geloescht !\n", x);
                    break;    

                case 'd' :  //EPROM - Hexdump
                    if (wait_for_ihex) break;
                    memchr(rx_buf, 0xFF, sizeof(rx_buf));

                    sz = get_eprom(rx_buf);

                    hexdump(rx_buf, sz);
                    break;   

                case 'p' : //EPROM programming
                    if (wait_for_ihex) break;
                    if (max_addr == 0) {
                        printf ("Datenpuffer leer\n");
                        break;
                    }
                    printf("EPROM brennen gestartet ...\n");
                    cyc = 0;
                    memchr(rx_buf, 0xFF, sizeof(rx_buf));

                    b = burn_eprom(buf_burn, &cyc);    //brennen & vergleichen
                    sleep_ms(100);
                    uint32_t sx = get_eprom(rx_buf); //kompl. Vergleichslesen mit gesenkter Vcc
                    bool z = true;
                    if (b) {
                        printf("Programmierung erfolgreich, max Zyklen: %d\n", cyc); 
                        for (int i=0; i<sx; i++) {
                            if (buf_burn[i] != 0xFF) {
                                if (buf_burn[i] != rx_buf[i]) {
                                    printf("Vergleichslesen fehlgeschlagen !!!\n");
                                    printf("1.Fehler - Adr: 0x%.4X\n", i);
                                    z = false;
                                    break;
                                }
                            }        
                        }
                        if (z) printf("Vergleichslesen erfolgreich\n");
                    }
                    else printf("Programmierung fehlgeschlagen, max Zyklen: %d\n", cyc);
                    break;  


                case 't' :  //Auswahl EPROM-Typ - ggf mit Argument t1 t2 t3 ...
                    if (wait_for_ihex) break;
                    rule_def_t pr;
                    if (arg == 0) {
                        printf("aktuelle Auswahl: ");
                        pr = rules_eprom();
                        printf("Typ: %s | ProgImpuls: %.3fms | Max Zyklen: %d\n", 
                            pr.name, pr.time_prog_plse_us / 1000.0, pr.max_cycles);
                        print_comp_types();    
                    } else {
                        if (arg < E_MAX_ENTRY) {
                            set_component(arg);
                            printf("ausgewaehlt: ");
                            pr = rules_eprom();
                            printf("Typ: %s | ProgImpuls: %.3fms | Max Zyklen: %d\n", 
                                pr.name, pr.time_prog_plse_us / 1000.0, pr.max_cycles);
                            } else {    //unzulässige Auswahl
                                printf("Unzulaessige Auswahl !!! ==> Auswahlmoeglichkeiten:\n");
                                print_comp_types();
                            }
                    
                    }
                    break;  

                case 'i' :  //Programmierimpulsdauer - Argument in µs
                    if (arg > 0) set_prog_time_us_eprom(arg);
                    else printf("Argument %d nicht zulaessig\n", arg);
                    pr = rules_eprom();
                    printf("Typ: %s | ProgImpuls: %.3fms | Max Zyklen: %d\n", 
                        pr.name, pr.time_prog_plse_us / 1000.0, pr.max_cycles);
                    break;

                case 'z' : //Anzahl der maximal zulässigen Programmierzyklen - Argument 1..99
                    if ((arg > 0) && (arg < 100)) set_max_cycles_eprom(arg);
                    else printf("Argument %d nicht zulaessig\n", arg);
                    pr = rules_eprom();
                    printf("Typ: %s | ProgImpuls: %.3fms | Max Zyklen: %d\n", 
                        pr.name, pr.time_prog_plse_us / 1000.0, pr.max_cycles);
                    break;                      

            }
            
        }
        else if (ferror(stdin))
            perror("Error");
    } 
}       

void set_component(enum components_t comp) {
    if ((comp > 0) && (comp < E_MAX_ENTRY)) component = comp;

    //Liste erweiterbar
    switch (comp) {
        case E_UNDEFINED:
            check_eprom = &check_dummy;
            get_eprom = &get_dummy;
            burn_eprom = &burn_dummy;
            rules_eprom = &rules_dummy;
            set_prog_time_us_eprom = &set_prog_time_us_dummy;
            set_max_cycles_eprom = &set_max_cycles_dummy;
            break;

        case E27C64:
            check_eprom = &check_27c64;
            get_eprom = &get_27c64;
            burn_eprom = &burn_27c64;
            rules_eprom = &rules_27c64;
            set_prog_time_us_eprom = &set_prog_time_us_27c64;
            set_max_cycles_eprom = &set_max_cycles_27c64;
            break;    

        case E27C128:
            check_eprom = &check_27c128;
            get_eprom = &get_27c128;
            burn_eprom = &burn_27c128;
            rules_eprom = &rules_27c128;
            set_prog_time_us_eprom = &set_prog_time_us_27c128;
            set_max_cycles_eprom = &set_max_cycles_27c128;
            break; 

        case E27C256:
            check_eprom = &check_27c256;
            get_eprom = &get_27c256;
            burn_eprom = &burn_27c256;
            rules_eprom = &rules_27c256;
            set_prog_time_us_eprom = &set_prog_time_us_27c256;
            set_max_cycles_eprom = &set_max_cycles_27c256;
            break;
    }

}

//Namen der verfügbaren Eproms als Liste ausgeben
void print_comp_types() {
    rule_def_t pr;
    enum components_t cx = component;
    for (int i=1; i<E_MAX_ENTRY; i++) { 
        set_component(i);
        pr = rules_eprom();
        printf("  --> t%d : %s\n", i, pr.name);
    }
    set_component(cx);                            
}

void hexdump(uint8_t *buf, uint32_t len) {
    uint16_t x = 0;
    uint16_t max_addr = len-1;
    while ( x < max_addr) {
        printf("%.4X ", x);
        for (int i=x; i<x+16; i++) {
            printf(" %.2X", buf[i]);
        }
        printf("\n");
        x += 16;
    }
}
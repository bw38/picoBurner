#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pico/stdlib.h"

#include "eproms.h"

// -----------------------------------------------------------------------

#define A00     0
#define A01     1
#define A02     2
#define A03     3
#define A04     4
#define A05     5
#define A06     6
#define A07     7
#define A08     8
#define A09     9
#define A10     10
#define A11     11
#define A12     12
#define A13     13

#define PGM     14
#define VPP     15      //Vpp 13V & Vcc 6V

#define D0      16
#define D1      17
#define D2      18
#define D3      19
#define D4      20
#define D5      21
#define D6      22
#define D7      26

#define CE      27
#define OE      28

//Adressmaske max 14 Bit
#define ADDR_MASK 1<<A00 | 1<<A01 | 1<<A02 | 1<< A03 | 1<<A04 | 1<<A05 | 1<<A06 | 1<<A07 |\
                  1<<A08 | 1<<A09 | 1<<A10 | 1<< A11 | 1<<A12 | 1<<A13

//Datenmaske 8 Bit
#define DATA_MASK 1<<D0 | 1<<D1 | 1<<D2 | 1<< D3 | 1<<D4 | 1<<D5 | 1<<D6 | 1<<D7                   

#define SZ_DATA 0x4000  //Speichergröße 16kB

//Programierbedingungen lt. Datenblatt, ggf anpassen
#define PROG_TIME_US    500 
#define MAX_PROG_CYCLES 20
#define COMP_NAME       "27C128"

static uint16_t prog_time_us = PROG_TIME_US; //Voreinstellung beim Start
static uint8_t  max_prog_cycles = MAX_PROG_CYCLES;

// -----------------------------------------------------------------------

static void io_init() {
    gpio_init(VPP);
    gpio_set_dir(VPP, GPIO_OUT);
    gpio_put(VPP, 0);

    gpio_init(CE);
    gpio_set_dir(CE, GPIO_OUT);
    gpio_put(CE, 1);    

    gpio_init(OE);
    gpio_set_dir(OE, GPIO_OUT);
    gpio_put(OE, 1);

    gpio_init(PGM);
    gpio_set_dir(PGM, GPIO_OUT);
    gpio_put(PGM, 1);

    gpio_init_mask(ADDR_MASK);
    gpio_set_dir_out_masked(ADDR_MASK);
    gpio_put_masked(ADDR_MASK, 0);
    gpio_init_mask(DATA_MASK);
    gpio_set_dir_in_masked(DATA_MASK);   
}

//Adressen von GPIO fortlaufend, keine Sortierung notwendig
static void io_set_adr(uint16_t adr){
    gpio_put_masked(ADDR_MASK, adr);
}

//Bereitstellen Daten an GPIO
static void io_set_data(uint8_t data) {
    gpio_set_dir_out_masked(DATA_MASK);
    uint32_t res = 0; 
    res |= (data & 0x01) << D0; data >>= 1;
    res |= (data & 0x01) << D1; data >>= 1;
    res |= (data & 0x01) << D2; data >>= 1;
    res |= (data & 0x01) << D3; data >>= 1;
    res |= (data & 0x01) << D4; data >>= 1;
    res |= (data & 0x01) << D5; data >>= 1;
    res |= (data & 0x01) << D6; data >>= 1;
    res |= (data & 0x01) << D7; 
    gpio_put_masked(DATA_MASK, res);   
}

//Einlesen Daten von GPIO
static uint8_t io_get_data() {
    gpio_set_dir_in_masked(DATA_MASK);
    uint32_t x = gpio_get_all();
    uint8_t res = 0;
    res |= ((x >> D0) & 0x01) << 0;
    res |= ((x >> D1) & 0x01) << 1;
    res |= ((x >> D2) & 0x01) << 2;
    res |= ((x >> D3) & 0x01) << 3;
    res |= ((x >> D4) & 0x01) << 4;
    res |= ((x >> D5) & 0x01) << 5;
    res |= ((x >> D6) & 0x01) << 6;
    res |= ((x >> D7) & 0x01) << 7;
    return res;
}

// ----------------------------------------------------------

//Anzahl nicht gelöschter Bytes (!=0xFF) ermitteln
int check_27c128() {
    int res = 0;
    io_init();
    gpio_put(CE, 0);
    gpio_put(OE, 0);
    sleep_us(10);
    for (int i=0; i<SZ_DATA; i++) {
        io_set_adr(i);
        sleep_us(10);
        if (io_get_data() != 0xFF) res++;
    }
    gpio_put(CE, 1);
    gpio_put(OE, 1);
    return res;
}

//EPROM-Inhalt zurückliefern
//Aufrufer gibt buf wieder frei
uint32_t get_27c128(uint8_t *buf) {
    io_init();
    gpio_put(CE, 0);
    gpio_put(OE, 0);
    sleep_us(100);
    for (int i=0; i<SZ_DATA; i++) {
        io_set_adr(i);
        sleep_us(10);
        buf[i] = io_get_data();
    }
    gpio_put(CE, 1);
    gpio_put(OE, 1);
    return SZ_DATA;
}

//EPROM brennen
//Soll-Bytes == 0xFF werden übersprungen
//Return: Anzahl der max.benötigten Prog-Impulse
//ausreichender zeitlicher Abstand zwischen debn Vorgängen für alle Typen
bool burn_27c128(uint8_t *buf, int *cycles) {
    bool b;
    *cycles = 0;
    io_init();
    gpio_put(VPP, 1);   //13V Vpp & 6V Vcc
    gpio_put(CE, 0);
    sleep_ms(10);

    for (int i = 0; i<SZ_DATA; i++) {
        if (buf[i] != 0xFF) {
            io_set_adr(i);
            int x = 0;
            b = false;
            while (x++ < max_prog_cycles) { //max Prog-Impulse lt. Datenblatt
                io_set_data(buf[i]);
                sleep_us(25);               //Sicherheit
                //Programmierimpuls
                gpio_put(PGM, 0);            //CE=0  OE=1
                sleep_us(prog_time_us);     //Dauer lt. Datenblatt
                gpio_put(PGM, 1);            //Ende Progimpuls
                sleep_us(25);
                //Vergleichslesen
                gpio_set_dir_in_masked(DATA_MASK);
                gpio_put(OE, 0);            //CE=1  OE=0
                sleep_us(25); 
                uint8_t y = io_get_data();
                b = (buf[i] == y);
                if (f_debug)
                    printf("0x%.4X | Soll: 0x%.2X | Ist: 0x%.2X | Cyc: %d | Res: %d\n", i, buf[i], y, x, b); 
                gpio_put(OE, 1);
                if (b) break;               //Bei Erfolg vorzeitig beenden
            }
            if (*cycles < x) *cycles = x;
            if (!b) break;                  //Fehler beim Vergleichslesen
        }
    }
    gpio_put(VPP, 0);   //Vpp 0V & Vcc 5V via Diode
    gpio_put(CE, 1);
    return b;
}

//Rücklieferung Informationen zur Programmiervorschrift / Typ
rule_def_t rules_27c128() {
    rule_def_t r;
    bzero(r.name, sizeof(r.name));
    memcpy(r.name, COMP_NAME, sizeof(COMP_NAME));
    r.time_prog_plse_us = prog_time_us;
    r.max_cycles = max_prog_cycles;
    return r;
}

//Ändern der voreingesellten Programmierimpulsdauer
void set_prog_time_us_27c128 (uint16_t time_us) {
    prog_time_us = time_us;
}

//Ändern der voreingestellten maximalen Programmierzyklen
void set_max_cycles_27c128 (uint8_t cyc) {
    max_prog_cycles = cyc;
}

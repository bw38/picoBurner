#ifndef IO_CONTROL_H
#define IO_CONTROL_H

static bool f_debug = false;

//Liste der vorhandenen Programmiervorschriften, erweiterbar
enum components_t  {
    E_UNDEFINED,
    E27C64,
    E27C128,
    E27C256,

    E_MAX_ENTRY
};

//f. Return Programmierregel
typedef struct  {
    char name[32];
    int  time_prog_plse_us;
    int  max_cycles;
} rule_def_t;

//Dummy f. fehlende Auswahl
int check_dummy();
uint32_t get_dummy(uint8_t *buf);
bool burn_dummy(uint8_t *buf, int *cycles);
rule_def_t rules_dummy();
void set_prog_time_us_dummy (uint16_t time_us);
void set_max_cycles_dummy (uint8_t cyc);

// Programmiervorschriften -------------------------------

//8k - 27C64
int check_27c64();
uint32_t get_27c64(uint8_t *buf);
bool burn_27c64(uint8_t *buf, int *cycles);
rule_def_t rules_27c64();
void set_prog_time_us_27c64 (uint16_t time_us);
void set_max_cycles_27c64 (uint8_t cyc);

//16k - 27C128
int check_27c128();
uint32_t get_27c128(uint8_t *buf);
bool burn_27c128(uint8_t *buf, int *cycles);
rule_def_t rules_27c128();
void set_prog_time_us_27c128 (uint16_t time_us);
void set_max_cycles_27c128 (uint8_t cyc);

//32k - 27C256
int check_27c256();
uint32_t get_27c256(uint8_t *buf);
bool burn_27c256(uint8_t *buf, int *cycles);
rule_def_t rules_27c256();
void set_prog_time_us_27c256 (uint16_t time_us);
void set_max_cycles_27c256 (uint8_t cyc);

// --------------------------------------------------------

//Sprungverteiler f. Standard-Funktionen
static int (*check_eprom)(void);
static uint32_t (*get_eprom)(uint8_t *buf);
static bool (*burn_eprom)(uint8_t *buf, int *cycles);
static rule_def_t (*rules_eprom)();
static void (*set_prog_time_us_eprom)(uint16_t time_us);
static void (*set_max_cycles_eprom)(uint8_t cyc);


#endif